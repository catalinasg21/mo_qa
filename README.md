# DOCUMENTOS DE PROYECTOS:

[Inicio](https://gitlab.com/catalinasg21/mo_qa/)

---

Sitio para centralizar todas la documentación generada al momento de ejecutar los diferentes procesos a nivel de pruebas.

## CONTENIDO:

- [MO-Production](https://gitlab.com/catalinasg21/mo_qa/-/tree/main/MO-Production)
- [MO-Colombia](https://gitlab.com/catalinasg21/mo_qa/-/tree/main/MO-Colombia)
- [MO-Peru](https://gitlab.com/catalinasg21/mo_qa/-/tree/main/MO-Peru)
- [MO-Mexico](https://gitlab.com/catalinasg21/mo_qa/-/tree/main/MO-Mexico)
- [MO-Ecuador](https://gitlab.com/catalinasg21/mo_qa/-/tree/main/MO-Ecuador)
- [MO-Usa](https://gitlab.com/catalinasg21/mo_qa/-/tree/main/MO-usa)
- [MO-Rdominicana](https://gitlab.com/catalinasg21/mo_qa/-/tree/main/MO-Rdominicana)
- [MO-Argentina](https://gitlab.com/catalinasg21/mo_qa/-/tree/main/MO-Argentina)
- [MO-Global](https://gitlab.com/catalinasg21/mo_qa/-/tree/main/MO-Global)

---

## ¿QUÉ DOCUMENTAR?
Cada proyecto puede tener una información diferente y depende del tipo de proyecto que se estipule en los requerimientos.

## ¿CÓMO DOCUMENTAR?
Estamos migrando nuestra documentación a la word, ya que la idea es tener muy consiso los flujos o comando que se usan al momento de la ejecución de las pruebas, para los diferentes tipos de diagramas que se lleguen a colocar  utilizaremos algunas herramientas:
* Drawio

### EN ESTE REPOSITORTIO DEBE ESTAR:
* Flujos importantes del producto.
* Tareas manuales o que se ejecutan en las pruebas.





